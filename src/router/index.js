import Vue from 'vue'
import Router from 'vue-router'
import MainLayout from 'components/layouts/main-layout'
import HelloWorld from 'components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: MainLayout,
      children: [
        {
          path: 'hello',
          component: HelloWorld
        }
      ]
    }
  ]
})
